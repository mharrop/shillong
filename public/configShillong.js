const configShillong = {
  "searchUrl": "https://sdad.omeka.net/items/browse?collection=20&public=true&sort_field=Dublin+Core%2CTitle&advanced[0][joiner]=and&advanced[0][element_id]=99&advanced[0][type]=is+not+empty&advanced[0][terms]",
  "baseURL": "https://sdad.omeka.net",
  "mapwarperDotNetIDs": [
    "77675",
    "77678",
    "77707"
  ],
  "mapwarperDotNetLabels": [
    "1880 Sketch Map of Shillong",
    "Shillong Guide Map 1925",
    "Shillong Guide Map 1946"
  ],
  "mapwarperProvIDs": [
    ""
  ],
  "mapwarperProvLabels": [
    ""
  ],
  "mapCentre": "25.582222,91.894444",
  "mapZoom": "12",
  "stubArticles": true,
  "mapURL": "https://mharrop.pages.gitlab.unimelb.edu.au/shillong/"
};