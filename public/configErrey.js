const configErrey = {
  "searchUrl": "https://errey.omeka.net/items/browse?search=&advanced%5B0%5D%5Bjoiner%5D=and&advanced%5B0%5D%5Belement_id%5D=&advanced%5B0%5D%5Btype%5D=&advanced%5B0%5D%5Bterms%5D=&range=&collection=3",
  "baseURL": "https://errey.omeka.net",
  "mapwarperDotNetIDs": [
    "14327"
  ],
  "mapwarperDotNetLabels": [
    "Random Mapwarper.net map"
  ],
  "mapwarperProvIDs": [
    "7228",
    "7957",
    "11700",
    "4409",
    "7658",
    "5124"
  ],
  "mapwarperProvLabels": [
    "PROV map One",
    "PROV map Two",
    "PROV map Three",
    "PROV map Four",
    "PROV map Five",
    "PROV map Six"
  ],
  "mapCentre": "-37,144",
  "mapZoom": "5",
  "stubArticles": true,
  "mapURL": "https://mharrop.pages.gitlab.unimelb.edu.au/shillong/"
};