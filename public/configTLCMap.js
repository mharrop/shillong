const configTLCMap = {
  "searchUrl": "https://ghap.tlcmap.org/places?format=json&fuzzyname=Melbourne&state=VIC",
  "baseURL": "https://ghap.tlcmap.org",
  "mapwarperDotNetIDs": [
    "80116",
    "80117",
    "80119",
    "80120"
  ],
  "mapwarperDotNetLabels": [
    "Bibbs Map PROV c1857-67",
    "MMBW 1018_bw0009",
    "Insurance Co Map 1915_122-123_MNE 18.2 R ",
    "Morgans 1935.md1952-133-b01-y001 "
  ],
  "mapCentre": "-37.814, 144.96332",
  "mapZoom": "14",
  "stubArticles": true,
  "mapURL": "https://mharrop.pages.gitlab.unimelb.edu.au/tlcmap"
};
